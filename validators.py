from typing import Optional


GENDER_MAP = {
    'male': 'Мужской',
    'female': 'Женский',
    'none': 'Не уточняю',
}


def gender_hru(gender: int) -> Optional[str]:
    return GENDER_MAP.get(gender)


def validate_age(text: str) -> Optional[int]:
    try:
        age = int(text)
    except (TypeError, ValueError):
        return None

    if age < 0 or age > 100:
        return None
    return age