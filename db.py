import sqlite3

def ensure_connection(func):
    def inner(*args, **kwargs):
        with sqlite3.connect('form.db') as conn:
            kwargs['conn'] = conn
            res = func(*args, **kwargs)
        return res

    return inner


@ensure_connection
def init_db(conn, force: bool = False):

    c = conn.cursor()

    if force:
        c.execute('DROP TABLE IF EXISTS user_message')
    
    c.execute('''
        CREATE TABLE IF NOT EXISTS user_message (
            id          INTEGER PRIMARY KEY,
            user_id     INTEGER NOT NULL,
            name        TEXT NOT NULL,
            text        TEXT NOT NULL
        )
    ''')
    
    conn.commit()

    
@ensure_connection
def add_message(conn, user_id: int, name: str, text: str):
    c = conn.cursor()
    c.execute('INSERT INTO user_message (user_id, name, text) VALUES (?, ?, ?)', (user_id, name, text))
    conn.commit()


@ensure_connection
def count_messages(conn, user_id: int):
    c = conn.cursor()
    c.execute('SELECT COUNT(*) FROM user_message WHERE user_id = ? LIMIT 1', (user_id, ))
    (result, ) = c.fetchone()
    return result


@ensure_connection
def list_messages(conn, user_id: int, limit: int = 10):
    c = conn.cursor()
    c.execute('SELECT id, text FROM user_message WHERE user_id = ? ORDER BY id DESC LIMIT ? ', (user_id, limit))
    return c.fetchall()


if __name__ == '__main__':
    init_db()

#     add_message(user_id = 1234, name = 'Arstan', text ='nice')
    
#     r = count_messages(user_id = 1234)
#     print(r)

    # r = list_messages(user_id = 399249143, limit = 4)
    # for i in r:
    #     print(i)
    # print(r)