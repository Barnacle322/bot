from telegram import Bot
from telegram import Update
from telegram import ReplyKeyboardMarkup
from telegram import KeyboardButton
from telegram import ReplyKeyboardRemove
from telegram import InlineKeyboardButton
from telegram import InlineKeyboardMarkup
from telegram.ext import CallbackQueryHandler
from telegram.ext import CallbackContext
from telegram.ext import Updater
from telegram.ext import MessageHandler
from telegram.ext import Filters
from telegram.ext import CommandHandler
from telegram.ext import ConversationHandler
from telegram.utils.request import Request
from pyowm import OWM
from pyowm.utils.config import get_default_config

CITY = range(1)
weatherButton = "Погода"
owm = OWM('2d32f0f4840b07cc5727026c57a45927')

config_dict = get_default_config()
config_dict['language'] = 'ru' 

def starter(update: Update, context: CallbackContext):
    update.message.reply_text(
        text = f'Привет. Это телеграм бот разработаный на питоне, использующий базу данных sqllite для хранения данных.\n\nДля отображения меню используйте /menu'
    )

def weatherButton_handler(update: Update, context: CallbackContext):
    update.message.reply_text(
        text = 'Укажите город или страну'
    )
    return CITY

def cancel_handler(update: Update, context: CallbackContext):
    update.message.reply_text(
        text = 'Отмена. Для начала с нуля нажмите /start'
    )
    return ConversationHandler.END

def button_page(update: Update, context: CallbackContext):
    reply_markup = ReplyKeyboardMarkup(
        resize_keyboard=True,
        keyboard = [
            [
                KeyboardButton(text = weatherButton),
            ],
        ]
    )

    update.message.reply_text(
        text = 'Выберите команду',
        reply_markup = reply_markup, 
    )

# Conv handler для работы погоды
def city_handler(update: Update, context: CallbackContext):
        # Получить имя
    context.user_data[CITY] = update.message.text
    if context.user_data[CITY] == '/cancel':
        return cancel_handler
    else:
        try:
            place = context.user_data[CITY]

            mgr = owm.weather_manager()
            observation = mgr.weather_at_place(place)
            w = observation.weather

            temp = w.temperature('celsius')['temp']
            update.message.reply_text(
            text = f'В городе {context.user_data[CITY]} сейчас {w.detailed_status} {temp}° по цельсию'
        )
            name = update.effective_user.first_name
            print(f'{name}: "{context.user_data[CITY]}"')
        except:
            update.message.reply_text(
                text = "Введите название без ошибок"
            )
    return ConversationHandler.END    

# Логика работы кнопок на альт клаве и добавление сообщений в базу
def message_handler(update: Update, context: CallbackContext):
    
    # Вывод сообщений и отправителя в терминал
    text = update.effective_message.text
    user = update.effective_user

    if user:
        name = user.first_name
    else:
        name = 'Аноним'
    print(f'{name}: "{text}"')

    # Перенаправление на хэндлеры по контексту
    if text == weatherButton:
        return weatherButton_handler(update = update, context = context)
    else:
        pass

    # Отправить сообщение с меню
    update.message.reply_text(
        text = f'Добро пожалось в погодный бот, нажмите в меню на кнопку "Погода".\n\nДля открытия меню пропишите /menu', 
    )
conv_handler = ConversationHandler(
    entry_points=  [
            MessageHandler(filters = Filters.text('Погода'), callback = weatherButton_handler),
            # CommandHandler('startform', weatherButton_handler),
        ],
        states={
            CITY: [
                MessageHandler(Filters.all, city_handler, pass_user_data = True)
            ],
            # FINISH: [
            #     MessageHandler(Filters.all, finish_handler2, pass_user_data = True)
            # ]
        },
        fallbacks = [
            CommandHandler('cancel', cancel_handler)
        ]
    )

def main():

    req = Request(
        connect_timeout=0.5,
        read_timeout=1.0,
    )

    bot = Bot(
        token='1310620692:AAHPziavWRZItgrbFAgWpg3UHJhTeeqaCYg',
        request=req,
    )

    updater = Updater(
        bot=bot,
        use_context=True,
    )

    # Подключение к базе данных

    # Обработчик сообщений
    # conv_handler = ConversationHandler(
    #     entry_points=[
    #         CallbackQueryHandler(start_handler, pass_user_data=True),
    #     ],
    #     states={
    #         NAME: [
    #             MessageHandler(Filters.all, name_handler, pass_user_data=True),
    #         ],
    #         GENDER: [
    #             CallbackQueryHandler(age_handler, pass_user_data=True),
    #         ],
    #         AGE: [
    #             MessageHandler(Filters.all, finish_handler, pass_user_data=True),
    #         ],
    #     },
    #     fallbacks=[
    #         CommandHandler('cancel', cancel_handler),
    #     ],
    # )
    updater.dispatcher.add_handler(conv_handler)
    # updater.dispatcher.add_handler(conv_handler2)
    # updater.dispatcher.add_handler(CommandHandler('cancel', cancel_handler))
    updater.dispatcher.add_handler(CommandHandler(filters = Filters.command, command = 'start', callback = starter))
    updater.dispatcher.add_handler(CommandHandler(filters = Filters.command, command = "menu", callback = button_page))
    updater.dispatcher.add_handler(MessageHandler(filters = Filters.text, callback = message_handler))
    # updater.dispatcher.add_handler(CallbackQueryHandler(callback_handler))

    # Обработка входящих сообщений
    updater.start_polling()
    updater.idle()

# Запуск
if __name__ == '__main__':
    main()