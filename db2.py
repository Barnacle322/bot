import sqlite3

def ensure_connectionn(func):
    def inner(*args, **kwargs):
        with sqlite3.connect('form2.db') as conn:
            kwargs['conn'] = conn
            res = func(*args, **kwargs)
        return res

    return inner


@ensure_connectionn
def init_db(conn, force: bool = False):

    c = conn.cursor()

    if force:
        c.execute('DROP TABLE IF EXISTS user_form')
    
    c.execute('''
        CREATE TABLE IF NOT EXISTS user_form (
            id          INTEGER PRIMARY KEY,
            name        TEXT NOT NULL,
            gender      TEXT NOT NULL,
            age         INTEGER NOT NULL
        )
    ''')
    
    conn.commit()

    
@ensure_connectionn
def add_form(conn, name: str, gender: str, age: str):
    c = conn.cursor()
    c.execute('INSERT INTO user_form (name, gender, age) VALUES (?, ?, ?)', (name, gender, age))
    conn.commit()


@ensure_connectionn
def list_messages1(conn, name: str, limit: int = 10):
    c = conn.cursor()
    c.execute('SELECT id, text FROM user_form WHERE name = ? ORDER BY id DESC LIMIT ? ', (name, limit))
    return c.fetchall()

# if __name__ == '__main__':
#     init_db(force = True)

#     add_message(name = "Arstan", gender = "Male", age = 17)