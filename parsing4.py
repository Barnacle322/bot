import requests
from bs4 import BeautifulSoup

def get_page_soup(url):
    res = requests.get(url)
    soup = BeautifulSoup(res.text, 'lxml')
    return soup


def get_items_info(soup):
    table = soup.find('div', class_ = 'kurs-bar__rates').find_all('tr')
    List = []
    List2 = []
    for i in table:
        try:
            elements = i.findChildren('td')[0].text
            elements2 = i.findChildren('td')[1].text
            List.append(elements)
            List2.append(elements2)
            
        except:
            pass
    
    Keys = ['usd_buy', 'eur_buy', 'rub_buy', 'kzt_buy', 'cny_buy', 'gbp_buy']
    del List[6:12]
    Keys2 = ['usd_sell', 'eur_sell', 'rub_sell', 'kzt_sell', 'cny_sell', 'gbp_sell']
    del List2[6:12]
    
    zipbObj = zip(Keys, List)
    dictOfWords = dict(zipbObj)

    zipbObj2 = zip(Keys2, List2)
    dictOfWords2 = dict(zipbObj2)

    return dictOfWords, dictOfWords2


def main_parser():
    page_url = 'https://www.valuta.kg'
    page_soup = get_page_soup(page_url)
    products = get_items_info(page_soup)
    return products
    

if __name__ == '__main__':
    main_parser()