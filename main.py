# Импортируется библиотека телеграм
from pyowm import OWM
from pyowm.utils.config import get_default_config
from telegram import (
    Bot, InlineKeyboardButton, InlineKeyboardMarkup, KeyboardButton,
    ReplyKeyboardMarkup, ReplyKeyboardRemove, Update)
from telegram.ext import (CallbackContext, CallbackQueryHandler,
                          CommandHandler, ConversationHandler, Filters,
                          MessageHandler, Updater, JobQueue)
from telegram.utils.request import Request
from telegram.ext.dispatcher import run_async

from apikeys import *
# Импортируются функции из файла db.py
from db import add_message, count_messages, init_db, list_messages
from db2 import add_form, list_messages1
import db3
from parsing import parsing_main
from parsing2 import parsing_main2
from parsing3 import parsing_main3
from validators import GENDER_MAP, gender_hru, validate_age
from datetime import time
from parsing4 import main_parser
from parsing5 import parse
from parsing6 import Client
# Задаются некоторые переменные для дальнешей работы
owm = OWM(OWMTOKEN)
config_dict = get_default_config()
config_dict['language'] = 'ru'

COMMAND_COUNT = 'count'
COMMAND_LIST = 'list'
CALLBACK_RESET ='reset'
CALLBACK_SAVE = 'save'
NAME, GENDER, AGE = range(3)
CITY = range(1)
ANIME = range(1)
MANGA = range(1)
REMOVE = range(1)
ADD = range(1)

button_help = "Справка"
button_anime_seasonal = "Аниме этого сезона"
button_registration = "Отзыв"
button_weather = "Погода"
button_anime_list = 'Anime List'
button_manga_list = 'Manga List'
nextButton = "->"
nextButton2 = "-->"
nextButton3 = "--->"
backButton = "<-"
backButton2 = "<--"
backButton3 = '<---'
nextButton_bermet = "---->"
backButton_bermet = "<----"
nextButton_bermet2 = "➡"
backButton_bermet2 = "⬅"
backButton_emir = '<'
nextButton_emir = '>'
nextButton_arthur = '=>'
nextButton_arthur2 = '==>'
backButton_arthur = '<='
backButton_arthur2 = '<=='
button_creator = "Создатели"
button_contacts = "Контакты"
button_source_code = "Исходный код"

button_emir = "Новости Спорта"
button_bermet = "Курс Валют"
button_arthur = "Reddit"
button_arstan = "Аниме"

# Бермет
RATE = range(1)
RATE2 = range(1)
RATE3 = range(1)
RATE4 = range(1)
RATE5 = range(1)
button_help2 = "Справка"
button_rates = "Курс валют сейчас"
button_kgs_conv = "Конвертация KGS в валюты"
button_usd_conv = "Конвертация USD в KGS"
button_eur_conv = "Конвертация EUR в KGS"
button_rub_conv = "Конвертация RUB в KGS"
button_kzt_conv = "Конвертация KZT в KGS"

# Эмир
button_need_help = 'Нужна помощь!'
button_no_thanks = 'Нет, спасибо!'
button_site = 'Сайт'
button_news = 'Свежая новость'

# Артур
button_add = 'Добавить Сабреддит'
button_remove = 'Удалить Сабреддит'
button_latest_news = 'Последние посты'
button_cancel = 'Отменить'

# /Start
def starter(update: Update, context: CallbackContext):
    update.message.reply_text(
        text = f'Привет. Этот телеграм бот, разработаный на питоне, использует базу данных sqllite для хранения данных.\n\nДля отображения меню используйте /menu. Для открытия меню по базе данных отправьте любое сообщение'
    )

# Функции для работы кнопок
def button_handler(update: Update, context: CallbackContext):
    update.message.reply_text(
        text = 'Данный функциональный бот был разработан командой Avalon',
        # reply_markup = ReplyKeyboardRemove(),
    ) 

def button2_handler(update: Update, context: CallbackContext):
    func = parsing_main()
    title = func['Title']
    title_translated = title.translate(str.maketrans({'.': r'\.', '!': r'\!', '(': r'\(', ')': r'\)', '_': r'\_', '-': r'\-'}))
    synopsis = func['Synopsis']
    synopsis_translated = synopsis.translate(str.maketrans({'.': r'\.', '!': r'\!', '(': r'\(', ')': r'\)', '_': r'\_', '-': r'\-'}))
    image = func['Image']
    link = func['Link']
    rating = func['Score'].replace('\n', '').strip(' ').lstrip(' ').replace('.', r'\.')
    text = f'[{title_translated}]({link})\n\n{synopsis_translated.ljust(900)[:900].strip()}\.\.\.\n\n⭐{rating}\n\n\(Requests\)'
    print(text)
    bot.send_photo(chat_id = update.message.chat_id, photo = image, caption = text, parse_mode = 'MarkdownV2')

    # update.message.reply_text(
    #     text = text,
    #     # reply_markup = ReplyKeyboardRemove(),
    # )

def button1_page2_handler(update: Update, context: CallbackContext):
    update.message.reply_text(
        text = "Бот был разработан Артуром, Арстаном, Бермет, Эмиром и Адилетом",
        # reply_markup = ReplyKeyboardRemove(),
    )

def button2_page2_handler(update: Update, context: CallbackContext):
    update.message.reply_text(
        text = "Обучение проходило в https://it-academy.kg/\n\nОтзывы по боту оставлять @Barnacle",
        # reply_markup = ReplyKeyboardRemove(),
    )

def button3_page2_handler(update: Update, context: CallbackContext):
    text = f'[Исходный код](https://bitbucket.org/Barnacle322/bot/src/master/)'
    bot.send_message(chat_id = update.message.chat_id, text = text, parse_mode = 'MarkdownV2')

def manga_button_handler(update: Update, context: CallbackContext):
    update.message.reply_text(
        text = 'Отправьте ник сайта myanimelist.net и вы получите первые 90 вхождений\n\nПожалуйста, подождите немного после отправки запроса, бот отвечает не моментально(Selenium)'
    )
    return MANGA

def manga_list_handler(update: Update, context: CallbackContext):
    context.user_data[MANGA] = update.message.text

    if context.user_data[MANGA] == '/cancel':
        return cancel_handler
    else:
        try:
            user = update.effective_user
            if user:
                name = user.first_name
            else:
                name = 'Аноним'
            print(f'{name}: "{context.user_data[MANGA]}"')

            result = parsing_main3(context.user_data[MANGA])
            print('result executed')

            bs_dot = r'\.'
            bs_exc = r'\!'
            bs_obr = r'\('
            bs_cbr = r'\)'
            bs_und = r'\_'
            bs_das = r'\-'
            bs_osb = r'\['
            bs_csb = r'\]'

            text = '\n\n'.join([f"[{manga_title.translate(str.maketrans({'.': bs_dot, '!': bs_exc, '(': bs_obr, ')': bs_cbr, '_': bs_und, '-': bs_das, '[': bs_osb, ']': bs_csb}))}]({manga_link}) \- {manga_status}" for manga_title, manga_link, manga_status in result])
            print('text executed')
            bot.send_message(chat_id = update.message.chat_id, text = text, parse_mode = 'MarkdownV2')
            print('send message executed')
        except:
            update.message.reply_text(
                text = "Произошла ошибка"
            )
    return ConversationHandler.END

# Бермет--------------------------------------------------------
def button_help_handler(update: Update, context: CallbackContext):
    update.message.reply_text(
        text='Этот бот отображает актуальный курс валют, средний по Бишкеку за последние 2 часа. Информация берется с сайта www.valuta.kg'
    )

def rates_handler(update: Update, context: CallbackContext):
    result = main_parser()
    buy = result[0]
    sell = result[1]
    text0 = f'$ покупка = {buy.get("usd_buy")}\n$ продажа = {sell.get("usd_sell")}\n\n€ покупка = {buy.get("eur_buy")}\n€ продажа = {sell.get("eur_sell")}\n\n₽ покупка = {buy.get("rub_buy")}\n₽ продажа = {sell.get("rub_sell")}\n\n'
    text = f'{text0}₸ покупка = {buy.get("kzt_buy")}\n₸ продажа = {sell.get("kzt_sell")}\n\nCNY покупка = {buy.get("cny_buy")}\nCNY продажа = {sell.get("cny_sell")}\n\nGBP покупка = {buy.get("gbp_buy")}\nGBP продажа = {sell.get("gbp_sell")}'
    update.message.reply_text(
        text=text
    )

def kgs_converter_handler(update: Update, context: CallbackContext):
    update.message.reply_text(
        text = 'Введи сумму в сомах для конвертации в разные валюты (USD, EUR, RUB, KZT)'
    )
    return RATE2

def kgs_converter(update: Update, context: CallbackContext):
    chat = update.effective_chat
    text = update.message.text
    try:
        func = main_parser()[1]
        print(func)
        number2 = float(text)
        rate = func.get('usd_sell')
        rate_eur = func.get('eur_sell')
        rate_kzt = func.get('kzt_sell')
        rate_rub = func.get('rub_sell')
        dollars = number2 / float(rate)
        dollars = round(dollars, 2)
        euros = number2 / float(rate_eur)
        euros = round(euros, 2)
        kzt = number2 / float(rate_kzt)
        kzt = round(kzt, 2)
        rubles = number2 / float(rate_rub)
        rubles = round(rubles, 2)
        message = f'KGS {number2} = $ {dollars}\n\nKGS {number2} = € {euros}\n\nKGS {number2} = ₸ {kzt}\n\nKGS {number2} = ₽ {rubles}'
        context.bot.send_message(chat_id=chat.id, text=message)
    except:
	    context.bot.send_message(chat_id=chat.id, text="Для конвертациииии необходимо ввести число")

    return ConversationHandler.END

def usd_converter_handler(update: Update, context: CallbackContext):
    update.message.reply_text(
        text = 'Введите сумму в долларах для конвертации в сомы'
    )
    return RATE

def usd_converter(update: Update, context: CallbackContext):
    chat = update.effective_chat
    text = update.message.text
    try:
        number = float(text)
        rate = main_parser()[0].get('usd_buy')        
        soms = number * float(rate)
        message = "$ %.2f = KGS %.2f" % (number, soms)
        context.bot.send_message(chat_id=chat.id, text=message)
    except:
	    context.bot.send_message(chat_id=chat.id, text="Для конвертации необходимо ввести число")
    return ConversationHandler.END

def eur_converter_handler(update: Update, context: CallbackContext):
    update.message.reply_text(
        text = 'Введи сумму в евро для конвертации в сомы'
    )
    return RATE3

def eur_converter(update: Update, context: CallbackContext):
    chat = update.effective_chat
    text = update.message.text
    try:
        number = float(text)
        rate = main_parser()[0].get('eur_buy')        
        soms = number * float(rate)
        message = "€ %.2f = KGS %.2f" % (number, soms)
        context.bot.send_message(chat_id=chat.id, text=message)
    except:
	    context.bot.send_message(chat_id=chat.id, text="Для конвертации необходимо ввести число")
    return ConversationHandler.END

def rub_converter_handler(update: Update, context: CallbackContext):
    update.message.reply_text(
        text = 'Введи сумму в рублях для конвертации в сомы'
    )
    return RATE4

def rub_converter(update: Update, context: CallbackContext):
    chat = update.effective_chat
    text = update.message.text
    try:
        number = float(text)
        rate = main_parser()[0].get('rub_buy')        
        soms = number * float(rate)
        message = "₽ %.2f = KGS %.2f" % (number, soms)
        context.bot.send_message(chat_id=chat.id, text=message)
    except:
	    context.bot.send_message(chat_id=chat.id, text="Для конвертации необходимо ввести число")
    return ConversationHandler.END

def kzt_converter_handler(update: Update, context: CallbackContext):
    update.message.reply_text(
        text = 'Введи сумму в тенге для конвертации в сомы'
    )
    return RATE5

def kzt_converter(update: Update, context: CallbackContext):
    chat = update.effective_chat
    text = update.message.text
    try:
        number = float(text)
        rate = main_parser()[0].get('kzt_buy')        
        soms = number * float(rate)
        message = "₸ %.2f = KGS %.2f" % (number, soms)
        context.bot.send_message(chat_id=chat.id, text=message)
    except:
	    context.bot.send_message(chat_id=chat.id, text="Для конвертации необходимо ввести число")
    return ConversationHandler.END

# Эмир-------------------------------------------------------
def button_need_help_handler(update: Update, context: CallbackContext):
    update.message.reply_text(
        text = 'Можете обратиться по номеру'
    )

def button_no_thanks_handler(update: Update, context: CallbackContext):
    update.message.reply_text(
        text = '👍'
    )

def button_site_handler(update: Update, context: CallbackContext):
    update.message.reply_text(
        text = 'https://footballhd.ru/allnews/'
    )

def button_news_handler(update: Update, context: CallbackContext):
    result = parse()
    update.message.reply_text(
        text = result
    )

# Артур----------------------------------------------------
def start(update: Update, context: CallbackContext):
    reply_markup2 = button_page2_arthur(update = update, context = context)
    update.message.reply_text(
        text=f'Вас приветствует Reddit-bot от команды Авалон! \n'
             f'              Выберите действие',
        reply_markup=markup
    )

def add(update: Update, context: CallbackContext):
    reply_markup2 = button_page3_arthur(update = update, context = context)
    update.message.reply_text(
        text=f'Чтобы добавить Саббредит - скиньте боту ссылку! \n',
        reply_markup = reply_markup2
    )

    return ADD

def add_save(update: Update, context: CallbackContext):
    user = update.effective_user
    text = update.effective_message.text
    reply_markup = button_page2_arthur(update = update, context = context)
    if user:
        name = user.first_name
    else:
        name = 'Аноним'
    if 'reddit.com' in text:
        if text:
            db3.add_message3(
                user_id=user.id,
                text=text,
                name=name,
            )    
            update.message.reply_text(
                text=f'Сабреддит сохранён! \n',
                reply_markup = reply_markup
            )
    else:
        update.message.reply_text(
            text = "Отправьте сслыку на сабреддит вида 'reddit.com/r/subreddit"
        )
    return ConversationHandler.END

def remove(update: Update, context: CallbackContext):
    user = update.effective_user
    reply_markup2 = button_page3_arthur(update = update, context = context)

    update.message.reply_text(
        text=f'Введите цифру Сабреддита, который хотите удалить: \n',
    )

    messages = db3.list_messages3(user_id=user.id)
    parse_urls = '\n'.join([f'{message_id} - {text}' for message_id, text in messages])
    update.message.reply_text(
        text=parse_urls,
        reply_markup = reply_markup2
    )

    return REMOVE

def remove_saves(update: Update, context: CallbackContext):
    text = update.effective_message.text
    user = update.effective_user
    reply_markup = button_page2_arthur(update = update, context = context)
    messages = db3.list_messages3(user_id=user.id)
    List = []

    for x in messages:
        y = x[0]
        List.append(y)
    try:
        if text.isdigit() and int(text) in List:
            db3.delete_db3(
                id=text,
            )

            update.message.reply_text(
                text=f'Сабреддит удалён! \n',
                reply_markup=reply_markup,
            )
        else:
            update.message.reply_text(
                text=f'Введите верный ID',
                reply_markup=reply_markup
    )
    except:
        update.message.reply_text(
            text=f'Произошла ошибка',
            reply_markup=reply_markup
    )
    return ConversationHandler.END

def new(update: Update, context: CallbackContext):
    user = update.effective_user
    messages = db3.list_messages2(user_id=user.id)
    List = []

    for x in messages:
        y = x[0]
        List.append(y)

    List2= []

    for i in List:
        if 'reddit.com' in i:
            List2.append(i)
    if List2 == []:
        update.message.reply_text(
            text = "Нет ссылок на сабреддиты"
        )
    else:
        for urls in List2:
            urls = 'http://'+urls

            text = Client().run(urls)
            bs_dot = r'\.'
            bs_exc = r'\!'
            bs_obr = r'\('
            bs_cbr = r'\)'
            bs_und = r'\_'
            bs_das = r'\-'
            bs_osb = r'\['
            bs_csb = r'\]'

            url = text[2]
            title = text[0].translate(str.maketrans(
                {'.': bs_dot, '!': bs_exc, '(': bs_obr, ')': bs_cbr, '_': bs_und, '-': bs_das, '[': bs_osb, ']': bs_csb}))
            description = text[1]

            if description == None:
                text = f'[{title}]({url})'
            else:
                text = f"[{title}]({url})\n\n{description.translate(str.maketrans({'.': bs_dot, '!': bs_exc, '(': bs_obr, ')': bs_cbr, '_': bs_und, '-': bs_das, '[': bs_osb, ']': bs_csb}))}"

            bot.send_message(chat_id=update.message.chat_id, text=text, parse_mode='MarkdownV2')

# Entry point для conv handler'a аниме списка
def anime_button_handler(update: Update, context: CallbackContext):
    update.message.reply_text(
        text = 'Отправьте ник сайта myanimelist.net и вы получите первые 90 вхождений\n\nПожалуйста, подождите немного после отправки запроса, бот отвечает не моментально(Selenium)'
    )
    return ANIME

# Ответ для конв хэндлера
def anime_list_handler(update: Update, context: CallbackContext):
    context.user_data[ANIME] = update.message.text

    if context.user_data[ANIME] == '/cancel':
        return cancel_handler
    else:
        try:
            user = update.effective_user
            if user:
                name = user.first_name
            else:
                name = 'Аноним'
            print(f'{name}: "{context.user_data[ANIME]}"')

            result = parsing_main2(context.user_data[ANIME])
            print('result executed')
            bs_dot = r'\.'
            bs_exc = r'\!'
            bs_obr = r'\('
            bs_cbr = r'\)'
            bs_und = r'\_'
            bs_das = r'\-'
            bs_osb = r'\['
            bs_csb = r'\]'
            
            text = '\n\n'.join([f"[{anime_title.translate(str.maketrans({'.': bs_dot, '!': bs_exc, '(': bs_obr, ')': bs_cbr, '_': bs_und, '-': bs_das, '[': bs_osb, ']': bs_csb}))}]({anime_link}) \- {anime_status}" for anime_title, anime_link, anime_status in result])
            print('text executed')
            bot.send_message(chat_id = update.message.chat_id, text = text, parse_mode = 'MarkdownV2')
            print('send message executed')
        except:
            update.message.reply_text(
                text = "Произошла ошибка"
            )
    return ConversationHandler.END    

# Entry point для conv handler'a погоды
def weatherButton_handler(update: Update, context: CallbackContext):
    update.message.reply_text(
        text = 'Укажите город или страну(PyOWM)'
    )
    return CITY

# Ответ для конв хэндлера
def city_handler(update: Update, context: CallbackContext):
    user = update.effective_user.first_name    
        # Получить имя
    context.user_data[CITY] = update.message.text
    bot.send_message(-452229638, (f'{user} ищет погоду в {context.user_data[CITY]}'))
    if context.user_data[CITY] == '/cancel':
        return cancel_handler
    else:
        try:
            place = context.user_data[CITY]

            mgr = owm.weather_manager()
            observation = mgr.weather_at_place(place)
            w = observation.weather

            temp = w.temperature('celsius')['temp']
            update.message.reply_text(
            text = f'В городе 🌆 {place} сейчас {w.detailed_status} {temp}° по цельсию'
        )
        except:
            update.message.reply_text(
                text = "Произошла ошибка"
            )
    return ConversationHandler.END    

def callback_alarm(context: CallbackContext):
  bot.send_message(chat_id=id, text='Hi, This is a daily reminder')

def reminder(update: Update,context: CallbackContext):
   bot.send_message(chat_id = update.effective_chat.id , text='Daily reminder has been set! You\'ll get notified at 8 AM daily')
   JobQueue.run_daily(callback = callback_alarm, context=update.message.chat_id,days=(0, 1, 2, 3, 4, 5, 6),time = time(hour = 18, minute = 45))

# Entry point для conv handler'a регистрации
def start_handler(update: Update, context: CallbackContext):
    # Спросить имя
    update.message.reply_text(
        text='Оставьте свой отзыв тут(SQLLite)',
    )
    return NAME 

# Ответы конв хэндлера
def name_handler(update: Update, context: CallbackContext):
    # Получить имя
    context.user_data[NAME] = update.message.text
    if context.user_data[NAME] == '/cancel':
        return cancel_handler
    else:
        # Спросить пол
        inline_buttons = InlineKeyboardMarkup(
            inline_keyboard=[
                [
                    InlineKeyboardButton(text = GENDER_MAP["male"], callback_data = 'male'),
                    InlineKeyboardButton(text = GENDER_MAP["female"], callback_data = 'female'),
                    InlineKeyboardButton(text = GENDER_MAP["none"], callback_data = 'none')
                ],
            ],
        )
        update.message.reply_text(
            text='Выберите свой пол чтобы продолжить',
            reply_markup = inline_buttons,
        )
        return GENDER

def finish_handler(update: Update, context: CallbackContext):
    # Получить возраст
    age = validate_age(text=update.message.text)
    if age is None:
        update.message.reply_text('Пожалуйста, введите корректный возраст!')
        return AGE

    context.user_data[AGE] = age
    text = f'Вы считаете, что {context.user_data[NAME]}.\n\nВаш пол: {gender_hru(context.user_data[GENDER])} и вам {context.user_data[AGE]} лет\n\nСохранить?'
    # Завершить диалог
    update.message.reply_text(
        text = text,
        reply_markup = get_keyboard2(update = update, context = context)
    )
    bot.send_message(-452229638, f'Пришла заявка \n\n{text}')
    return ConversationHandler.END

# Отменяющая функция для конв хэндлеров
def cancel_handler(update: Update, context: CallbackContext):
    update.message.reply_text(
        text = 'Отмена'
    )
    return ConversationHandler.END

def cancel_handler2(update: Update, context: CallbackContext):
    reply_markup = button_page2_arthur(update = update, context = context)
    update.message.reply_text(
        text = button_cancel,
        reply_markup = reply_markup
    )
    return ConversationHandler.END

# Меню в альтернативной клавиатуре
def button_page1(update: Update, context: CallbackContext):
        # Логика отображения экранной клавиатуры с кнопками
    reply_markup = ReplyKeyboardMarkup(
        keyboard = [
            [
                KeyboardButton(text = button_emir),
                KeyboardButton(text = button_bermet),
                KeyboardButton(text = button_arthur)
            ],
            [
                KeyboardButton(text = nextButton),
            ]
        ],
        resize_keyboard = True,
    )

    update.message.reply_text(
        text = 'Выберите команду',
        reply_markup = reply_markup, 
    )

def button_page2(update: Update, context: CallbackContext):
    reply_markup = ReplyKeyboardMarkup(
        keyboard = [
            [
                
                KeyboardButton(text = button_help),
                KeyboardButton(text = button_source_code),
            ],
            [
                KeyboardButton(text = backButton),
                KeyboardButton(text = button_registration),
                KeyboardButton(text = nextButton2),
            ],
        ],
        resize_keyboard = True,
    )

    update.message.reply_text(
        text = 'Выберите команду',
        reply_markup = reply_markup, 
    )

def button_page3(update: Update, context: CallbackContext):
        # Логика отображения экранной клавиатуры с кнопками
    reply_markup = ReplyKeyboardMarkup(
        keyboard = [
            [

                KeyboardButton(text = button_weather),                
                KeyboardButton(text = button_creator),
                KeyboardButton(text = button_contacts),
            ],
            [
                KeyboardButton(text = backButton2),
                KeyboardButton(text = nextButton3),
            ]
        ],
        resize_keyboard = True,
    )

    update.message.reply_text(
        text = 'Выберите командуу',
        reply_markup = reply_markup, 
    )

def button_page4(update: Update, context: CallbackContext):
    reply_markup = ReplyKeyboardMarkup(
        resize_keyboard=True,
        keyboard = [
            [
                KeyboardButton(text = button_anime_seasonal),
                KeyboardButton(text = button_anime_list),
                KeyboardButton(text = button_manga_list),
            ],
            [
                KeyboardButton(text = backButton3)
            ]
        ]
    )

    update.message.reply_text(
        text = 'Выберите командуу',
        reply_markup = reply_markup, 
    )

def button_page1_bermet(update: Update, context: CallbackContext):
    # Логика отображения экранной клавиатуры с кнопками
    reply_markup = ReplyKeyboardMarkup(
        keyboard = [
            [
                KeyboardButton(text = button_emir),
                KeyboardButton(text = button_arstan),
                KeyboardButton(text = button_arthur)
            ],
            [
                KeyboardButton(text = nextButton_bermet),
            ]
        ],
        resize_keyboard = True,
    )

    update.message.reply_text(
        text = 'Сейчас вы находитесь в части Бермет',
        reply_markup = reply_markup, 
    )

def button_page2_bermet(update: Update, context: CallbackContext):
    reply_markup = ReplyKeyboardMarkup(
        keyboard=[
            [
                KeyboardButton(text=button_kgs_conv),
                KeyboardButton(text=button_usd_conv)
            ],
            [
                KeyboardButton(text=button_rates),
                KeyboardButton(text=backButton_bermet),
                KeyboardButton(text=nextButton_bermet2)
            ],
        ],
        resize_keyboard=True,
    )
    update.message.reply_text(
        text = 'Выбери команду', reply_markup = reply_markup,
    )

def button_page3_bermet(update: Update, context: CallbackContext):        
    reply_markup = ReplyKeyboardMarkup(
        keyboard=[
            [
                KeyboardButton(text=button_eur_conv),
                KeyboardButton(text=button_rub_conv)
            ],
            [
                KeyboardButton(text=button_kzt_conv),
                KeyboardButton(text=backButton_bermet2)                
            ],
        ],
        resize_keyboard=True,
    )
    update.message.reply_text(
        text = 'Выбери команду', reply_markup = reply_markup,
    )

def button_page1_arthur(update: Update, context: CallbackContext):
    # Логика отображения экранной клавиатуры с кнопками
    reply_markup = ReplyKeyboardMarkup(
        keyboard = [
            [
                KeyboardButton(text = button_emir),
                KeyboardButton(text = button_arstan),
                KeyboardButton(text = button_bermet)
            ],
            [
                KeyboardButton(text = nextButton_arthur),
            ]
        ],
        resize_keyboard = True,
    )

    update.message.reply_text(
        text = 'Сейчас вы находитесь в части Артура',
        reply_markup = reply_markup, 
    )

def button_page2_arthur(update: Update, context: CallbackContext):
    reply_markup = ReplyKeyboardMarkup(
        keyboard = [
            [
                KeyboardButton(text = button_add), 
                KeyboardButton(text = button_latest_news), 
                KeyboardButton(text = button_remove)
            ],
            [        
                KeyboardButton(text = backButton_arthur)
            ]
        ], 
        resize_keyboard=True, 
    )

    update.message.reply_text(
        text = 'Выберите команду',
        reply_markup = reply_markup, 
    )

def button_page3_arthur(update: Update, context: CallbackContext):
    reply_markup = ReplyKeyboardMarkup(
        [
            [
                KeyboardButton(text = button_cancel)
            ]
        ], 
        resize_keyboard=True,
    )
    
    update.message.reply_text(
        text = "Выберите команду",
        reply_markup = reply_markup, 
    )

def button_page1_emir(update: Update, context: CallbackContext):
    # Логика отображения экранной клавиатуры с кнопками
    reply_markup = ReplyKeyboardMarkup(
        keyboard = [
            [
                KeyboardButton(text = button_bermet),
                KeyboardButton(text = button_arstan),
                KeyboardButton(text = button_arthur)
            ],
            [
                KeyboardButton(text = nextButton_emir),
            ]
        ],
        resize_keyboard = True,
    )

    update.message.reply_text(
        text = 'Сейчас вы находитесь в части Эмира',
        reply_markup = reply_markup, 
    )

def button_page2_emir(update: Update, context: CallbackContext):
    reply_markup = ReplyKeyboardMarkup(
        keyboard = [
            [
                KeyboardButton(text = button_site),
                KeyboardButton(text = button_news),
            ],
            [
                KeyboardButton(text = backButton_emir),
            ]
        ],
        resize_keyboard = True,
    )

    update.message.reply_text(
        text = 'Выберите команду',
        reply_markup = reply_markup
    )

def button_help_handler_emir(update: Update, context: CallbackContext):
    reply_markup = ReplyKeyboardMarkup(
        keyboard = [
            [
                KeyboardButton(text = button_need_help),
                KeyboardButton(text = button_no_thanks),
            ],
        ],
        resize_keyboard = True,
        one_time_keyboard=True
    )

    update.message.reply_text(
        text = 'Есть какой-то вопрос?',
        reply_markup = reply_markup, 
    )

# Датабаза инлайн кнопки
def get_keyboard(update: Update, context: CallbackContext):
    return InlineKeyboardMarkup(
        inline_keyboard = [
            [
                InlineKeyboardButton(text = 'Кол-во сообщений', callback_data = COMMAND_COUNT),
            ],
            [
                InlineKeyboardButton(text = 'Мои сообщения', callback_data = COMMAND_LIST),
            ],
        ]
    )

# Регистрация сохранить промпт
def get_keyboard2(update: Update, context: CallbackContext):
    return InlineKeyboardMarkup(
        inline_keyboard = [
            [
                InlineKeyboardButton(text = 'Да', callback_data = CALLBACK_SAVE),
                InlineKeyboardButton(text = 'Нет, начать заново', callback_data = CALLBACK_RESET),
            ]
        ]
    )

# Логика работы кнопок на альт клаве и добавление сообщений в базу
@run_async
def message_handler(update: Update, context: CallbackContext):
    
    # Вывод сообщений и отправителя в терминал
    text = update.effective_message.text
    user = update.effective_user

    if user:
        name = user.first_name
    else:
        name = 'Аноним'
    print(f'{name}: "{text}"')

    # Перенаправление на хэндлеры по контексту
    if text == button_bermet:
        bot.send_message(-452229638, f'{name}: "{text}"')
        return button_page1_bermet(update = update, context = context)      

    if text == button_emir:
        bot.send_message(-452229638, f'{name}: "{text}"')
        return button_page1_emir(update = update, context = context)   

    if text == button_arthur:
        bot.send_message(-452229638, f'{name}: "{text}"')
        return button_page1_arthur(update = update, context = context)

    if text == button_arstan:
        bot.send_message(-452229638, f'{name}: "{text}"')
        return button_page1(update = update, context = context)   

    if text == button_help:
        bot.send_message(-452229638, f'{name}: "{text}"')
        return button_handler(update = update, context = context)

    elif text == button_anime_seasonal:
        bot.send_message(-452229638, f'{name}: "{text}"')
        return button2_handler(update = update, context = CallbackContext)

    elif text == nextButton:
        return button_page2(update = update, context = context)

    elif text == backButton:
        return button_page1(update = update, context = context)

    elif text == button_creator:
        bot.send_message(-452229638, f'{name}: "{text}"')
        return button1_page2_handler(update = update, context = context)

    elif text == button_contacts:
        bot.send_message(-452229638, f'{name}: "{text}"')
        return button2_page2_handler(update = update, context = context)

    elif text == button_source_code:
        bot.send_message(-452229638, f'{name}: "{text}"')
        return button3_page2_handler(update = update, context = context)
    
    elif text == nextButton2:
        return button_page3(update = update, context = context)
    
    elif text == backButton2:
        return button_page2(update = update, context = context)
    
    elif text == nextButton3:
        return button_page4(update = update, context = context)

    elif text == backButton3:
        return button_page3(update = update, context = context)

    elif text == button_anime_list:
        return anime_button_handler(update = update, context = context)
    
    elif text == button_manga_list:
        return manga_button_handler(update = update, context = context)

    # Бермет
    elif text == button_help2:
        return button_help_handler(update = update, context = context)  

    elif text == button_rates:
        return rates_handler(update = update, context = context)

    elif text == nextButton_bermet:
        return button_page2_bermet(update = update, context = context)

    elif text == nextButton_bermet2:
        return button_page3_bermet(update = update, context = context)

    elif text == backButton_bermet:
        return button_page1_bermet(update = update, context = context)
    
    elif text == backButton_bermet2:
        return button_page2_bermet(update = update, context = context)

    # Эмир
    elif text == button_need_help:
        return button_need_help_handler(update = update, context = context), button_page2_emir(update = update, context = context)

    elif text == button_no_thanks:
        return button_no_thanks_handler(update = update, context = context), button_page2_emir(update = update, context = context)

    elif text == button_site:
        return button_site_handler(update = update, context = context)

    elif text == button_news:
        return button_news_handler(update = update, context = context)

    elif text == backButton_emir:
        return button_page1_emir(update = update, context = context)
     
    elif text == nextButton_emir:
        return button_page2_emir(update = update, context = context)

    # Артур 

    elif text == nextButton_arthur:
        return button_page2_arthur(update = update, context = context)

    elif text == backButton_arthur:
        return button_page1_arthur(update = update, context = context)

    elif text == button_latest_news:
        return new(update = update, context = context)

    elif text == button_cancel:
        return button_page2_arthur(update = update, context = context)

    # Отправить сообщение с меню
    update.message.reply_text(
        text = 'Доступ к вашим сообщениям в базе данных(SQLLite)',
        reply_markup = get_keyboard(update = update, context = context), 
    )
    bot.send_message(-452229638, f'{name}: "{text}"')
    # Добавить сообщение в базу данных
    if text:
        add_message(
            user_id = user.id,
            name = name,
            text = text,
        )
    else:
        pass

# Функция отвечающая за работу меню у сообщений
def callback_handler(update: Update, context: CallbackContext):
    
    user = update.effective_user
    callback_data = update.callback_query.data
    
    # База данных
    if callback_data == COMMAND_COUNT:
        count = count_messages(user_id = user.id)
        text = f'У вас {count} сообщений'

    elif callback_data == COMMAND_LIST:
        messages = list_messages(user_id=user.id, limit=5)
        text = '\n\n'.join([f'#{message_id} - {message_text}' for message_id, message_text in messages])
    
    # Регистрация
    elif callback_data == CALLBACK_SAVE:
        text = 'Ваши данные сохранены в базе данных'

    # Добавление в базу данных
        add_form(
            name = context.user_data[NAME],
            gender = gender_hru(context.user_data[GENDER]),
            age = context.user_data[AGE],
        )
    
    elif callback_data == CALLBACK_RESET:
        text = 'Отмена. Для начала с нуля нажмите /start'

    # Спросить пол
    elif callback_data == 'male' or 'female' or 'none':
        # Получить пол
        if callback_data not in GENDER_MAP:
            # Этой ситуации не должно быть для пользователя! То есть какое-то значение
            # в кнопках есть, но оно не включено в список гендеров
            update.effective_message.reply_text('Что-то пошло не так, обратитесь к администратору бота')
            return GENDER

        context.user_data[GENDER] = callback_data

        # Спросить возраст
        update.effective_message.reply_text(
            text='Введите свой возраст:',
        )
        return AGE

    else:
        text = 'Произошла ошибка'

    # Ответ для функционала базы данных
    update.effective_message.reply_text(
        text = text,
    )
    print(f'{user.first_name} выбрал "{callback_data}"')
    bot.send_message(-452229638, f'{user.first_name} выбрал "{callback_data}"')

# Основа для конв хэндлера для регистрации
conv_handler = ConversationHandler(
    entry_points=[
            CommandHandler('startform', start_handler),
            MessageHandler(filters = Filters.text(button_registration), callback = start_handler)
        ],
        states={
            NAME: [
                MessageHandler(Filters.all, name_handler, pass_user_data=True),
            ],
            GENDER: [
                CallbackQueryHandler(callback_handler, pass_user_data=True),
            ],
            AGE: [
                MessageHandler(Filters.all, finish_handler, pass_user_data=True),
            ],
        },
        fallbacks=[
            CommandHandler('cancel', cancel_handler),
        ],
    )
# Основа для конв хэндлера для погоды
conv_handler2 = ConversationHandler(
    entry_points=  [
            MessageHandler(filters = Filters.text(button_weather), callback = weatherButton_handler),
            CommandHandler('startform', weatherButton_handler),
        ],
        states={
            CITY: [
                MessageHandler(Filters.all, city_handler, pass_user_data = True)
            ],
        },
        fallbacks = [
            CommandHandler('cancel', cancel_handler)
        ]
    )

conv_handler3 = ConversationHandler(
    entry_points=  [
            MessageHandler(filters = Filters.text(button_anime_list), callback = anime_button_handler),
        ],
        states={
            ANIME: [
                MessageHandler(Filters.all, anime_list_handler, pass_user_data = True)
            ],
        },
        fallbacks = [
            CommandHandler('cancel', cancel_handler)
        ]
    )

conv_handler4 = ConversationHandler(
    entry_points=  [
            MessageHandler(filters = Filters.text(button_manga_list), callback = manga_button_handler),
        ],
        states={
            MANGA: [
                MessageHandler(Filters.all, manga_list_handler, pass_user_data = True)
            ],
        },
        fallbacks = [
            CommandHandler('cancel', cancel_handler)
        ]
    )

# Бермет
conv_handler5 = ConversationHandler(
    entry_points=  [
            MessageHandler(filters = Filters.text(button_usd_conv), callback = usd_converter_handler),
        ],
        states={
            RATE: [
                MessageHandler(Filters.all, usd_converter, pass_user_data = True)
            ],
        },
        fallbacks = [
            CommandHandler('cancel', cancel_handler)
        ]
    )

conv_handler6 = ConversationHandler(
    entry_points=  [
            MessageHandler(filters = Filters.text(button_kgs_conv), callback = kgs_converter_handler),
        ],
        states={
            RATE2: [
                MessageHandler(Filters.all, kgs_converter, pass_user_data = True)
            ],
        },
        fallbacks = [
            CommandHandler('cancel', cancel_handler)
        ]
    )

conv_handler7 = ConversationHandler(
    entry_points=  [
            MessageHandler(filters = Filters.text(button_eur_conv), callback = eur_converter_handler),
        ],
        states={
            RATE3: [
                MessageHandler(Filters.all, eur_converter, pass_user_data = True)
            ],
        },
        fallbacks = [
            CommandHandler('cancel', cancel_handler)
        ]
    )

conv_handler8 = ConversationHandler(
    entry_points=  [
            MessageHandler(filters = Filters.text(button_rub_conv), callback = rub_converter_handler),
        ],
        states={
            RATE4: [
                MessageHandler(Filters.all, rub_converter, pass_user_data = True)
            ],
        },
        fallbacks = [
            CommandHandler('cancel', cancel_handler)
        ]
    )

conv_handler9 = ConversationHandler(
    entry_points=  [
            MessageHandler(filters = Filters.text(button_kzt_conv), callback = kzt_converter_handler),
        ],
        states={
            RATE5: [
                MessageHandler(Filters.all, kzt_converter, pass_user_data = True)
            ],
        },
        fallbacks = [
            CommandHandler('cancel', cancel_handler)
        ]
    )

# Артур
conv_handler10 = ConversationHandler(
    entry_points=[
        MessageHandler(filters=Filters.text(button_add), callback=add)
        ],
        states={
            ADD: [
                MessageHandler(filters = Filters.text, callback = add_save)
            ]
        },
        fallbacks=[
            CommandHandler('cancel', cancel_handler2),
            MessageHandler(filters=Filters.text(button_cancel), callback=cancel_handler2)
        ]
    )

conv_handler11 = ConversationHandler(
    entry_points=[
        MessageHandler(filters=Filters.text(button_remove), callback=remove)
        ],
        states={
            REMOVE: [
                MessageHandler(filters = Filters.text, callback = remove_saves)
            ]
        },
        fallbacks=[
            CommandHandler('cancel', cancel_handler2),
            MessageHandler(filters = Filters.text(button_cancel), callback = cancel_handler2)
        ]
    )

# Настройка и диспэтчеры
def main():

    req = Request(
        connect_timeout=0.5,
        read_timeout=1.0,
    )
    global bot
    bot = Bot(
        token=TOKEN,
        request=req,
    )

    updater = Updater(
        bot=bot,
        use_context=True,
    )

    # Подключение к базе данных
    init_db()
    dp = updater.dispatcher

    dp.add_handler(conv_handler)
    dp.add_handler(conv_handler2)
    dp.add_handler(conv_handler3)
    dp.add_handler(conv_handler4)

    # Бермет
    dp.add_handler(conv_handler5)
    dp.add_handler(conv_handler6)
    dp.add_handler(conv_handler7)
    dp.add_handler(conv_handler8)
    dp.add_handler(conv_handler9)

    # Артур 
    dp.add_handler(conv_handler10)
    dp.add_handler(conv_handler11)

    dp.add_handler(CommandHandler('cancel', cancel_handler))
    dp.add_handler(CommandHandler(filters = Filters.text, command = 'help', callback = button_help_handler_emir))
    dp.add_handler(CommandHandler(filters = Filters.command, command = 'start', callback = starter))
    dp.add_handler(CommandHandler(filters = Filters.command, command = "menu", callback = button_page1))
    dp.add_handler(MessageHandler(filters = Filters.text, callback = message_handler))

    dp.add_handler(CallbackQueryHandler(callback_handler))

    # Обработка входящих сообщений
    updater.start_polling()
    updater.idle()

# Запуск
if __name__ == '__main__':
    main()