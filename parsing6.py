
import requests
import bs4
import logging

# logging.basicConfig(level=logging.DEBUG)
logger = logging.getLogger('Reddit')

class Client:
    def __init__(self):
        self.result = []
        self.session = requests.Session()
        self.session.headers = {
            'User-Agent': 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (kHTML, like Gecko) Chrome/85.0.4183.102 Safari/537.36'
        }


    def load_page(self, url1):
        url = url1
        res = self.session.get(url=url)
        res.raise_for_status()
        return res.text


    def parse_page(self, text: str):    # парсинг текста и ссылок
        soup = bs4.BeautifulSoup(text, 'lxml')

        title = soup.select_one('h3._eYtD2XCVieq6emjKBH3m')
        if not title:
            logger.error('no titles')
        else:
            title = title.text
            self.result.append(title)

        description = soup.select_one('p._1qeIAgB0cPwnLhDF9XSiJM')
        if not description:
            self.result.append(None)
        else:
            description = description.text
            self.result.append(description)

        url = soup.select_one('a.SQnoC3ObvgnGjWt90zD9Z._2INHSNB8V5eaWp4P0rY_mE')
        if not url:
            logger.error('no url')
        else:
            url = 'https://www.reddit.com' + url.get('href')
            self.result.append(url)



    def run(self, url1):
        text = self.load_page(url1)
        self.parse_page(text=text)

        return self.result


if __name__=='__main__':
    parsing = Client().run()


