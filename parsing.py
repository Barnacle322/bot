import requests
from bs4 import BeautifulSoup
import random

def get_page_soup(url):
    req = requests.get(url)
    soup = BeautifulSoup(req.text, 'lxml')
    return soup

def get_items_info(soup):
    # print(soup)
    products_list = soup.find('div', class_='seasonal-anime-list js-seasonal-anime-list js-seasonal-anime-list-key-1 clearfix')
    # print(products_list)
    products = products_list.find_all('div', class_='seasonal-anime js-seasonal-anime')
    products_data = []
    for anime in products:
        title = anime.find('a', class_='link-title').text
        synopsis = anime.find('div', class_='synopsis').text
        image = anime.find('div', class_='image').find('img').get('src')
        if image == None:
            image = anime.find('div', class_='image').find('img').get('data-src')        
        link = anime.find('div', class_='image').find('a').get('href')
        rating = anime.find('span', class_='score').text
        # price = anime.find('div', class_='listbox_price').text
        products_data.append({'Title': title.strip('\n'), 'Synopsis': synopsis.strip('\n').replace('\r\n\r\n',' ').replace('\r\n \r\n', ' '), 'Image': image, 'Link': link, 'Score': rating.replace('\n', '').strip(' ').lstrip(' ')})
    return products_data

def parsing_main():
    page_url = 'https://myanimelist.net/anime/season'
    page_soup = get_page_soup(page_url)
    products = get_items_info(page_soup)
    x = len(products) - 1
    randomnumber = random.randint(0, x)
    global parsing_result
    parsing_result = products[randomnumber]
    print(parsing_result)
    return parsing_result
    