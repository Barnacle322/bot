import requests
from bs4 import BeautifulSoup
import random

from selenium import webdriver
from selenium.webdriver.common.keys import Keys

PATH =r"C:\TelegramBot\ChromeDriver\chromedriver.exe"

def get_page_soup2(nickname):
    options = webdriver.ChromeOptions()
    options.add_argument("headless")
    driver = webdriver.Chrome(PATH, chrome_options=options)
    driver.get(f'https://myanimelist.net/animelist/{nickname}')
    soup = BeautifulSoup(driver.page_source, 'lxml')
    driver.close()
    return soup

def get_anime_list(soup):
    products_list = soup.find('table', attrs={'class': 'list-table'})
    products = products_list.find_all('tr', class_='list-table-data')
    products_data = []
    for anime in products:
        title = anime.find('td', class_='data title clearfix').find('a', class_ = 'link sort').text
        link = anime.find('td', class_='data title clearfix').find('a').get('href')
        status = anime.find('td').get('class')
        if status[2] == 'onhold':
            status.pop(2)
            status.append('on hold')
        # print(status)
        # synopsis = anime.find('div', class_='synopsis').text
        # image = anime.find('div', class_='image').find('img').get('src')
        # if image == None:
        #     image = anime.find('div', class_='image').find('img').get('data-src')        
        # link = anime.find('div', class_='image').find('a').get('href')
        # rating = anime.find('span', class_='score').text
        # price = anime.find('div', class_='listbox_price').text

        products_data.append([title, f'https://myanimelist.net{link}', status[2].replace('plan', 'plan ').replace('to', 'to ').capitalize()])
        # products_data.append({'Title': title.strip('\n'), 'Synopsis': synopsis.strip('\n').replace('\r\n\r\n',' ').replace('\r\n \r\n', ' '), 'Image': image, 'Link': link, 'Score': rating.replace('\n', '').strip(' ').lstrip(' ')})
    return products_data

def parsing_main2(nickname):
    # global nickname
    # nickname = str(input("Your nickname: "))
    page_soup = get_page_soup2(nickname)
    products = get_anime_list(page_soup)
    # print(products)
    return(products[0:89])

# if __name__ == "__main__":
#     parsing_main2()

# result = parsing_main2()

# text = '\n\n'.join([f'#{message_id} - {message_text}' for anime_title, anime_link, anime_status in result])
# print(driver.page_source)


# products_data = [{'Hello':1, 'Hello2':2}, {'Heeello': 3, 'Hell': 4}]
# products_data_new = []
# for dictionary in products_data:
#     print(type(dictionary))
#     List = list(dictionary.values())
#     products_data_new.append(List)
    
# print(products_data_new)