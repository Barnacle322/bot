import requests
from bs4 import BeautifulSoup
import random

from selenium import webdriver
from selenium.webdriver.common.keys import Keys

PATH =r"C:\TelegramBot\ChromeDriver\chromedriver.exe"

def get_page_soup3(nickname):
    options = webdriver.ChromeOptions()
    options.add_argument("headless")
    driver = webdriver.Chrome(PATH, chrome_options=options)
    driver.get(f'https://myanimelist.net/mangalist/{nickname}')
    soup = BeautifulSoup(driver.page_source, 'lxml')
    driver.close()
    return soup

def get_manga_list(soup):
    products_list = soup.find('table', attrs={'class': 'list-table'})
    products = products_list.find_all('tr', class_='list-table-data')
    products_data = []
    for manga in products:
        title = manga.find('td', class_='data title').find('a', class_ = 'link sort').text
        link = manga.find('td', class_='data title').find('a').get('href')
        status = manga.find('td').get('class')
        if status[2] == 'onhold':
            status.pop(2)
            status.append('on hold')
        products_data.append([title, f'https://myanimelist.net{link}', status[2].replace('plan', 'plan ').replace('to', 'to ').capitalize()])

    return products_data

def parsing_main3(nickname):
    # global nickname
    # nickname = str(input("Your nickname: "))
    page_soup = get_page_soup3(nickname)
    products = get_manga_list(page_soup)
    print(products)
    return(products)

if __name__ == "__main__":
    parsing_main3()