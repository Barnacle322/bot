import sqlite3

def ensure_connection(func):
    def inner(*args, **kwargs):
        with sqlite3.connect('form3.db') as conn:
            kwargs['conn'] = conn
            res = func(*args, **kwargs)
        return res

    return inner


@ensure_connection
def init_db(conn, force: bool = False):

    c = conn.cursor()

    if force:
        c.execute('DROP TABLE IF EXISTS user_message')
    
    c.execute('''
        CREATE TABLE IF NOT EXISTS user_message (
            id          INTEGER PRIMARY KEY,
            user_id     INTEGER NOT NULL,
            name        TEXT NOT NULL,
            text        TEXT NOT NULL
        )
    ''')
    
    conn.commit()

    
@ensure_connection
def add_message3(conn, user_id: int, name: str, text: str):
    c = conn.cursor()
    c.execute('INSERT INTO user_message (user_id, name, text) VALUES (?, ?, ?)', (user_id, name, text))
    conn.commit()


@ensure_connection
def list_messages3(conn, user_id: int, limit: int = 15):
    c = conn.cursor()
    c.execute('SELECT id, text FROM user_message WHERE user_id = ? ORDER BY id DESC LIMIT ? ', (user_id, limit))
    return c.fetchall()

@ensure_connection
def list_messages2(conn, user_id: int, limit: int = 15):
    c = conn.cursor()
    c.execute('SELECT text FROM user_message WHERE user_id = ? ORDER BY id DESC LIMIT ? ', (user_id, limit))
    return c.fetchall()

@ensure_connection
def delete_db3(conn, id: int):
    c = conn.cursor()
    c.execute('DELETE FROM user_message WHERE id = ?', (id,))
    conn.commit()


if __name__ == '__main__':
    init_db()
