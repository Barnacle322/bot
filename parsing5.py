import requests
from bs4 import BeautifulSoup

URL = 'https://footballhd.ru/allnews/'

def get_html(url, params=None):
    r = requests.get(url, params=params)
    return r


def get_teams(html):
    soup = BeautifulSoup(html, 'html.parser')
    items = soup.find_all('div', class_='news_grid')
    # items =soup.find_all('tr', class_='alt1')
    # print(items)




    news = []
    # scores_list = []
    # for score in items:
    #     score_item = score.find('td', class_='score').text
    #     scores_list.append(score_item)
    # print(scores_list[5])
    for item in items:
        news.append({
            'time': item.find('div', class_='sh_art').find('i', class_='date').text,
            'news': item.find('div', class_='sh_art').find('h2').text,
            'other': item.find('div', class_='sh_art').find('p').text,
            'link': item.find('div', class_='sh_art').find('h2').find('a').get('href'),
        })
    # print(news)
    # print(f'https://footballhd.ru{news[0].get("link")}')
    text = (f'Время: {news[0]["time"]}\nНовость: {news[0]["news"]}\nОписание: {news[0]["other"]}\nСсылка: https://footballhd.ru/{news[0]["link"]}')
    return text







# ------------------------------------------------------------
    # leicester = []
    # for item in items:
    #     leicester.append({
    #         'place': item.find('tr', class_='alt1').find('td', class_= 'name left').text,
    #         'name': item.find('tr', class_='alt1').find('td', class_= 'name left').text,
    #         'score': item.find('tr', class_='alt1').find('td', class_= 'score').text

    #         # 'scoreqwerty': item.find('tr', class_='alt1').find('td').text
    #         # 'points': item.find('td', class_='score').text

    #     })
    # print(leicester)



    # chelsea = []
    # for item in items:
    #     chelsea.append({
    #         'place': item.find('tr', class_='alt1').text,
    #         # 'place': item.find('tr', class_='alt1').find('td', class_= 'name left').text,
    #         # 'name': item.find('tr', class_='alt1').find('td', class_= 'name left').text,
    #         # 'score': item.find('tr', class_='alt1').find('td', class_= 'score').text

    #     })
    # print(chelsea)
    
    
    # column = []
    # for item in items:
    #     column.append({
    #         'name_table': item.find('title', class_='name left').text
    #     }) 
    # print(column)



    # column = []
    # for item in items:
    #     column.append({
    #         'name': item.find('table', class_='matchshedule').find('a').text,
    #         'place': item.find('tr', class_='color_turnirtable_place1').find('td').text #.find('a').text

    #     })
    # print(column)
    # print(len(column))
    


def parse():
    html = get_html(URL)
    parsing = get_teams(html.text)
    # if html.status_code == 200:
    #     get_teams(html.text)
    # else:
    #     print("Error")
    return parsing
    

# parse()






'''import requests
from bs4 import BeautifulSoup

URL = 'https://m.liveresult.ru/football/England/Premier-League/standings/'

def get_html(url, params=None):
    r = requests.get(url, params=params)
    return r


def get_teams(html):
    soup = BeautifulSoup(html, 'html.parser')
    items = soup.find_all('table', class_='maintbl standings-table')
    # print(items)


    leicester = []
    for item in items:
        leicester.append({
            'place': item.find('tr', class_='alt1').find('td', class_= 'name left').text,
            'name': item.find('tr', class_='alt1').find('td', class_= 'name left').text,
            'score': item.find('tr', class_='alt1').find('td', class_= 'score').text

            # 'scoreqwerty': item.find('tr', class_='alt1').find('td').text
            # 'points': item.find('td', class_='score').text

        })
    print(leicester)

    arsenal = []
    for item in items:
        arsenal.append({
            'place': item.find('tr', class_='alt2').find('td', class_= 'name left').text,
            'name': item.find('tr', class_='alt2').find('td', class_= 'name left').text,
            'score': item.find('tr', class_='alt2').find('td', class_= 'score').text

        })
    print(arsenal)
'''

